package com.danghai.bakamangareader;

public interface AppConfig {

    //Permission
    int MY_PERMISSIONS_REQUEST_READ_STORAGE = 101;
    int MY_PERMISSIONS_REQUEST_WRITE_STORAGE = 102;
    int REQUEST_CODE_GALLERY = 998;

    String DOMAIN_NAME = "http://192.168.1.109/mob403/";


    String LOGIN_URL  = DOMAIN_NAME + "login.php";
    String REGISTER_URL  = DOMAIN_NAME + "register.php";
    String LOGOUT_URL  = DOMAIN_NAME + "logout.php";

    //Get data
    String GET_CURRENT_USER_URL  = DOMAIN_NAME + "get_current_user.php";
    String GET_ALL_CATEGORY_URL  = DOMAIN_NAME + "get_all_category.php";
    String GET_NEW_MANGA_URL  = DOMAIN_NAME + "get_new_manga.php";
    String GET_UPDATE_MANGA_URL  = DOMAIN_NAME + "get_update_manga.php";
    String GET_MANGA_ORDER_BY_ID_URL  = DOMAIN_NAME + "get_manga_order_by_id.php";
    String GET_CHAPTER_ORDER_BY_MANGA_ID_URL  = DOMAIN_NAME + "get_chapter_order_by_manga_id.php";
    String GET_CATEGORY_ORDER_BY_MANGA_ID_URL  = DOMAIN_NAME + "get_category_order_by_manga_id.php";

    //Insert data

    String INSERT_CATEGORY_URL = DOMAIN_NAME + "insert_category.php";


    //Update data
    String UPDATE_CATEGORY_URL = DOMAIN_NAME + "update_category.php";
    String UPDATE_USER_INFORMATION_URL = DOMAIN_NAME + "update_user_information.php";

    //Delete data
    String REMOVE_DATA_URL  = DOMAIN_NAME + "remove_data.php";


}




