package com.danghai.bakamangareader.model;

import java.util.List;

public class Manga {
    private String id;
    private String title;
    private String content;
    private String thumbnail;
    private String cover;
    private String artist;
    private List<Category> category = null;
    private String createdAt;
    private Object updatedAt;
    private String views;


    public Manga(String id, String title, String thumbnail, String cover, List<Category> category, String createdAt, String views) {
        this.id = id;
        this.title = title;
        this.thumbnail = thumbnail;
        this.cover = cover;
        this.category = category;
        this.createdAt = createdAt;
        this.views = views;
    }

    public Manga(String id, String title, String content, String thumbnail, String cover, String artist, List<Category> category, String createdAt, Object updatedAt, String views) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.thumbnail = thumbnail;
        this.cover = cover;
        this.artist = artist;
        this.category = category;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.views = views;
    }

    public Manga() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public List<Category> getCategory() {
        return category;
    }

    public void setCategory(List<Category> category) {
        this.category = category;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Object getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Object updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getViews() {
        return views;
    }

    public void setViews(String views) {
        this.views = views;
    }
}
