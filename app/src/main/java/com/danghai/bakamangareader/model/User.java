package com.danghai.bakamangareader.model;

public class User {
    private int id;
    private String fullName;
    private String username;
    private String avatar;
    private String password;
    private String email;
    private int isOnline;
    private String createdAt;
    private int type;


    public User(int id, String fullName, String username, String avatar, String password, String email, int isOnline, String createdAt, int type) {
        this.id = id;
        this.fullName = fullName;
        this.username = username;
        this.avatar = avatar;
        this.password = password;
        this.email = email;
        this.isOnline = isOnline;
        this.createdAt = createdAt;
        this.type = type;
    }


    public User() {
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getIsOnline() {
        return isOnline;
    }

    public void setIsOnline(int isOnline) {
        this.isOnline = isOnline;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
}
