package com.danghai.bakamangareader.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.danghai.bakamangareader.R;
import com.danghai.bakamangareader.model.Category;
import com.danghai.bakamangareader.model.Chapter;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ChapterImageViewAdapter extends RecyclerView.Adapter<ChapterImageViewAdapter.ChapterImageHolder> {

    Context mContext;
    List<Chapter> mList;
    private OnItemClickListener mListener;

    public interface OnItemClickListener{
        void onItemClick(int position);
    }
    public void setOnItemClickListener(OnItemClickListener listener){
        this.mListener = listener;
    }

    public ChapterImageViewAdapter(Context mContext, List<Chapter> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    @NonNull
    @Override
    public ChapterImageViewAdapter.ChapterImageHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chapter_content, parent, false);

        return new ChapterImageViewAdapter.ChapterImageHolder(view, mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ChapterImageViewAdapter.ChapterImageHolder holder, int position) {
        Chapter chapter = mList.get(position);

        Picasso.get().load(chapter.getContent()).into(holder.chapterImage);

    }
    @Override
    public int getItemCount() {
        return mList.size();
    }
    public class ChapterImageHolder extends RecyclerView.ViewHolder {

        ImageView chapterImage;

        public ChapterImageHolder(@NonNull View itemView, final OnItemClickListener listener) {
            super(itemView);

            chapterImage = itemView.findViewById(R.id.iv_chapter_content_image);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null){
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION){
                            listener.onItemClick(position);
                        }
                    }
                }
            });
        }
    }
}
