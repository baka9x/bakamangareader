package com.danghai.bakamangareader.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.danghai.bakamangareader.R;
import com.danghai.bakamangareader.model.Category;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;
import java.util.List;

import static com.danghai.bakamangareader.AppConfig.DOMAIN_NAME;


public class CategorySmallItemAdapter extends RecyclerView.Adapter<CategorySmallItemAdapter.CategoryHolder> {

    Context mContext;
    List<Category> mList;
    private OnItemClickListener mListener;

    public interface OnItemClickListener{
        void onItemClick(int position);
    }
    public void setOnItemClickListener(OnItemClickListener listener){
        this.mListener = listener;
    }

    public CategorySmallItemAdapter(Context mContext, List<Category> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    @NonNull
    @Override
    public CategorySmallItemAdapter.CategoryHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_category_2, parent, false);

        return new CategorySmallItemAdapter.CategoryHolder(view, mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull CategorySmallItemAdapter.CategoryHolder holder, int position) {
        Category category = mList.get(position);
        holder.categoryName.setText(category.getName());

    }
    @Override
    public int getItemCount() {
        return mList.size();
    }
    public class CategoryHolder extends RecyclerView.ViewHolder {

        TextView categoryName;

        public CategoryHolder(@NonNull View itemView, final OnItemClickListener listener) {
            super(itemView);

            categoryName = itemView.findViewById(R.id.tv_sub_category_name);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null){
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION){
                            listener.onItemClick(position);
                        }
                    }
                }
            });
        }
    }
}
