package com.danghai.bakamangareader.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.danghai.bakamangareader.R;
import com.danghai.bakamangareader.model.Manga;
import com.squareup.picasso.Picasso;

import java.util.List;

import static com.danghai.bakamangareader.AppConfig.DOMAIN_NAME;

public class MangaRecyclerViewAdapter extends RecyclerView.Adapter<MangaRecyclerViewAdapter.MangaHolder> {
    Context mContext;
    List<Manga> mList;
    private OnItemClickListener mListener;

    public interface OnItemClickListener{
        void onItemClick(int position);
    }
    public void setOnItemClickListener(OnItemClickListener listener){
        this.mListener = listener;
    }
    public MangaRecyclerViewAdapter(Context mContext, List<Manga> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    @NonNull
    @Override
    public MangaRecyclerViewAdapter.MangaHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_new_manga, parent, false);
        return new MangaRecyclerViewAdapter.MangaHolder(view, mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull MangaRecyclerViewAdapter.MangaHolder holder, int position) {
        Manga manga = mList.get(position);
        holder.title.setText(manga.getTitle());
        Picasso.get().load(DOMAIN_NAME + manga.getThumbnail()).fit().into(holder.thumbnail);

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class MangaHolder extends RecyclerView.ViewHolder {
        ImageView thumbnail;
        TextView title;
        public MangaHolder(@NonNull View itemView, final OnItemClickListener listener) {
            super(itemView);
            thumbnail = itemView.findViewById(R.id.iv_manga_thumb);
            title = itemView.findViewById(R.id.tv_manga_title);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null){
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION){
                            listener.onItemClick(position);
                        }
                    }
                }
            });
        }
    }
}
