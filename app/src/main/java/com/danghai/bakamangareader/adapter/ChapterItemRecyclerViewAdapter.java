package com.danghai.bakamangareader.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.danghai.bakamangareader.R;
import com.danghai.bakamangareader.model.Chapter;
import com.squareup.picasso.Picasso;

import java.util.List;

import static com.danghai.bakamangareader.AppConfig.DOMAIN_NAME;

public class ChapterItemRecyclerViewAdapter extends RecyclerView.Adapter<ChapterItemRecyclerViewAdapter.ChapterHolder> {
    Context mContext;
    List<Chapter> mList;
    private OnItemClickListener mListener;
    public interface OnItemClickListener{
        void onItemClick(int position);
    }
    public void setOnItemClickListener(OnItemClickListener listener){
        this.mListener = listener;
    }

    public ChapterItemRecyclerViewAdapter(Context mContext, List<Chapter> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    @NonNull
    @Override
    public ChapterItemRecyclerViewAdapter.ChapterHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chapter, parent, false);
        return new ChapterItemRecyclerViewAdapter.ChapterHolder(view, mListener);
    }
    @Override
    public void onBindViewHolder(@NonNull ChapterItemRecyclerViewAdapter.ChapterHolder holder, int position) {
        Chapter chapter = mList.get(position);
        holder.title.setText(chapter.getTitle());
        holder.createdAt.setText(chapter.getCreatedAt());
        holder.views.setText(chapter.getViews());
        Picasso.get().load(DOMAIN_NAME + chapter.getThumbnail()).fit().into(holder.thumbnail);
    }
    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ChapterHolder extends RecyclerView.ViewHolder {
        ImageView thumbnail;
        TextView title, createdAt, views;

        public ChapterHolder(@NonNull View itemView, final OnItemClickListener listener) {
            super(itemView);
            thumbnail = itemView.findViewById(R.id.iv_chapter_thumbnail);
            title = itemView.findViewById(R.id.tv_chapter_title);
            createdAt = itemView.findViewById(R.id.tv_chapter_created_at);
            views = itemView.findViewById(R.id.tv_views);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null){
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION){
                            listener.onItemClick(position);
                        }
                    }
                }
            });
        }
    }
}
