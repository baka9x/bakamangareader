package com.danghai.bakamangareader.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.danghai.bakamangareader.R;
import com.google.android.material.textfield.TextInputLayout;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.app.Activity.RESULT_OK;
import static com.danghai.bakamangareader.AppConfig.DOMAIN_NAME;
import static com.danghai.bakamangareader.AppConfig.GET_CURRENT_USER_URL;
import static com.danghai.bakamangareader.AppConfig.REQUEST_CODE_GALLERY;
import static com.danghai.bakamangareader.AppConfig.UPDATE_USER_INFORMATION_URL;
import static com.danghai.bakamangareader.lib.ExtraBakaLibs.imageToString;


public class ProfileFragment extends Fragment {

    CircleImageView avatar;
    TextInputLayout fullname;
    TextView username;
    Button btnSave;


    SharedPreferences sharedPreferences;

    StringRequest stringRequest;
    RequestQueue requestQueue;
    String imageData;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        //Hooks
        init(view);
        getCurrentUserOnline();

        avatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openImage();
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateUserInformation();
            }
        });
        return view;
    }

    private void getCurrentUserOnline() {
        sharedPreferences = getContext().getSharedPreferences("LoginInfo", Context.MODE_PRIVATE);
        //Nạp thông tin lên form từ sharedPreference
        Boolean dataSave = sharedPreferences.getBoolean("login_information", false);
        if (dataSave) {
            username.setText(sharedPreferences.getString("username", ""));
        }
        stringRequest = new StringRequest(Request.Method.POST, GET_CURRENT_USER_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("dasdsadsa", response);
                try {
                    JSONArray items = new JSONArray(response);
                    for (int i = 0; i < items.length(); i++) {
                        JSONObject itemObj = items.getJSONObject(i);
                        String responseObj = itemObj.getString("response");
                        int statusObj = itemObj.getInt("status");
                        switch (statusObj){
                            case 403:
                                Toast.makeText(getContext(), "Not permission", Toast.LENGTH_SHORT).show();
                                break;
                            case 404:
                                Toast.makeText(getContext(), "No user data", Toast.LENGTH_SHORT).show();
                                break;
                            case 200:
                                JSONArray userItem = new JSONArray(responseObj);
                                for (int j = 0; j < userItem.length(); j++) {
                                    JSONObject userObj = userItem.getJSONObject(j);
                                    int id = userObj.getInt("id");
                                    String _fullname = userObj.getString("fullname");
                                    String _username = userObj.getString("username");
                                    String _email = userObj.getString("email");
                                    int _isOnline = userObj.getInt("is_online");
                                    String _createdAt = userObj.getString("created_at");
                                    int _type = userObj.getInt("type");
                                    String _avatar = userObj.getString("avatar");

                                    //Set data to layout
                                   fullname.getEditText().setText(_fullname);
                                   Picasso.get().load(DOMAIN_NAME + _avatar).fit().into(avatar);
                                }
                                break;

                        }

                    }



                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), "Error: "+ error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("username", sharedPreferences.getString("username", ""));
                return params;

            }
        };
        requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(stringRequest);
    }

    private void init(View view) {
        avatar = view.findViewById(R.id.profile_avatar);
        fullname = view.findViewById(R.id.profile_full_name);
        username = view.findViewById(R.id.profile_username);
        btnSave = view.findViewById(R.id.btn_save);


    }


    private void updateUserInformation() {

        stringRequest = new StringRequest(Request.Method.POST, UPDATE_USER_INFORMATION_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(getContext(), response, Toast.LENGTH_SHORT).show();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), "Error: "+ error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                if (imageData != null) {
                    params.put("image", imageData);
                }
                params.put("username", sharedPreferences.getString("username", ""));
                params.put("fullname", fullname.getEditText().getText().toString().trim());
                return params;
            }
        };
        requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(stringRequest);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == REQUEST_CODE_GALLERY) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent, REQUEST_CODE_GALLERY);
            } else {
                Toast.makeText(getContext(), "Error! You must have to permission!", Toast.LENGTH_SHORT).show();
            }
            return;
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        if (requestCode == REQUEST_CODE_GALLERY && resultCode == RESULT_OK && data != null) {

            CropImage.activity()
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .setAspectRatio(1, 1)
                    .start(getContext(), this);
        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);

            if (resultCode == RESULT_OK) {

                assert result != null;
                Uri resultUri = result.getUri();
                try {
                    InputStream inputStream = requireActivity().getApplicationContext().getContentResolver().openInputStream(resultUri);
                    Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                    avatar.setImageBitmap(bitmap);
                    imageData = imageToString(bitmap);


                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }

            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }



    private void openImage() {
        Intent galleryIntent = new Intent();
        galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        startActivityForResult(galleryIntent, REQUEST_CODE_GALLERY);
    }


}