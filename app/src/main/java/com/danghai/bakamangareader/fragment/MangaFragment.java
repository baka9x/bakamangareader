package com.danghai.bakamangareader.fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.danghai.bakamangareader.R;
import com.danghai.bakamangareader.adapter.CategorySmallItemAdapter;
import com.danghai.bakamangareader.adapter.ChapterItemRecyclerViewAdapter;
import com.danghai.bakamangareader.model.Category;
import com.danghai.bakamangareader.model.Chapter;
import com.google.android.material.textfield.TextInputLayout;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.danghai.bakamangareader.AppConfig.DOMAIN_NAME;
import static com.danghai.bakamangareader.AppConfig.GET_CATEGORY_ORDER_BY_MANGA_ID_URL;
import static com.danghai.bakamangareader.AppConfig.GET_CHAPTER_ORDER_BY_MANGA_ID_URL;


public class MangaFragment extends Fragment {

    ImageView thumbnail, cover;
    TextView title, artist, createdAt, summary;
    TextInputLayout edtComment;
    Button btnSend;
    RecyclerView rvCategoryList, rvChapterList, rvCommentList;

    List<Chapter> mChapter;
    List<Category> mCategory;
    ChapterItemRecyclerViewAdapter chapterItemRecyclerViewAdapter;
    CategorySmallItemAdapter categorySmallItemAdapter;
    StringRequest stringRequest;
    RequestQueue requestQueue;
    String categoryId;

    public MangaFragment() {
    }
    public MangaFragment(String categoryId){
        this.categoryId = categoryId;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_manga, container, false);
        //Hooks
        init(view);
        loadDataManga();
        return view;
    }

    private void loadDataManga() {
        Bundle bundle = getArguments();
        final String _manga_id = bundle.getString("MANGA_ID");
        String _manga_title = bundle.getString("MANGA_TITLE");
        String _manga_thumbnail = bundle.getString("MANGA_THUMBNAIL");
        String _manga_cover = bundle.getString("MANGA_COVER");
        String _manga_created_at = bundle.getString("MANGA_CREATED_AT");
        String _manga_content = bundle.getString("MANGA_CONTENT");
        String _manga_artist = bundle.getString("MANGA_ARTIST");
        //Load Data
        Picasso.get().load(DOMAIN_NAME+ _manga_thumbnail).fit().into(thumbnail);
        Picasso.get().load(DOMAIN_NAME+ _manga_cover).fit().into(cover);
        title.setText(_manga_title);
        createdAt.setText(_manga_created_at);
        summary.setText(_manga_content);
        artist.setText(_manga_artist);

        //Load category of manga
        stringRequest = new StringRequest(Request.Method.POST, GET_CATEGORY_ORDER_BY_MANGA_ID_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray items = new JSONArray(response);
                    for (int i = 0; i < items.length(); i++) {
                        JSONObject itemObj = items.getJSONObject(i);
                        String responseObj = itemObj.getString("response");
                        int statusObj = itemObj.getInt("status");
                        switch (statusObj){
                            case 403:
                                Toast.makeText(getContext(), "Not permission", Toast.LENGTH_SHORT).show();
                                break;
                            case 400:
                                Toast.makeText(getContext(), "Cannot load genre", Toast.LENGTH_SHORT).show();
                                break;
                            case 200:
                                mCategory = new ArrayList<>();
                                JSONArray categoryMangaItems = new JSONArray(responseObj);
                                for (int c = 0; c< categoryMangaItems.length(); c++) {
                                    JSONObject categoryMangaObj = categoryMangaItems.getJSONObject(c);
                                    String _cat_id = categoryMangaObj.getString("cat_id");
                                    String _cat_name = categoryMangaObj.getString("name");
                                    String _cat_des = categoryMangaObj.getString("des");
                                    String _cat_image = categoryMangaObj.getString("image");
                                    String _cat_created = categoryMangaObj.getString("created_at");
                                    String _cat_updated = categoryMangaObj.getString("updated_at");
                                    mCategory.add(new Category(_cat_id, _cat_name, _cat_des, _cat_image, _cat_created, _cat_updated));
                                    categorySmallItemAdapter = new CategorySmallItemAdapter(getContext(), mCategory);
                                    rvCategoryList.setAdapter(categorySmallItemAdapter);
                                    categorySmallItemAdapter.notifyDataSetChanged();
                                }
                                //Onclick
                                break;
                        }

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), "Error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("mangaid", _manga_id);
                return params;
            }
        };
        requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(stringRequest);

        //Load all chapter order by this manga id
        stringRequest = new StringRequest(Request.Method.POST, GET_CHAPTER_ORDER_BY_MANGA_ID_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray items = new JSONArray(response);
                    for (int i = 0; i < items.length(); i++) {
                        JSONObject itemObj = items.getJSONObject(i);
                        String responseObj = itemObj.getString("response");
                        int statusObj = itemObj.getInt("status");

                        switch (statusObj){
                            case 400:
                                Toast.makeText(getContext(), "No Chapter", Toast.LENGTH_SHORT).show();
                                break;
                            case 200:
                                mChapter = new ArrayList<>();
                                JSONArray chapterMangaItems = new JSONArray(responseObj);
                                for (int j = 0; j< chapterMangaItems.length(); j++) {
                                    JSONObject chapterMangaObj = chapterMangaItems.getJSONObject(j);
                                    String _chapter_id = chapterMangaObj.getString("chapter_id");
                                    String _chapter_title = chapterMangaObj.getString("chapter_title");
                                    String _chapter_thumbnail = chapterMangaObj.getString("chapter_thumbnail");
                                    String _chapter_content = chapterMangaObj.getString("chapter_content");
                                    String _chapter_created_at = chapterMangaObj.getString("chapter_created");
                                    String _chapter_updated_at = chapterMangaObj.getString("chapter_updated");
                                    String _manga_title = chapterMangaObj.getString("manga_title");
                                    String _chapter_views = chapterMangaObj.getString("chapter_views");

                                    mChapter.add(new Chapter(_chapter_id, _chapter_title, _chapter_thumbnail, _chapter_content, _chapter_created_at, _chapter_updated_at, _chapter_views, _manga_title));
                                    chapterItemRecyclerViewAdapter = new ChapterItemRecyclerViewAdapter(getContext(), mChapter);
                                    rvChapterList.setAdapter(chapterItemRecyclerViewAdapter);
                                    chapterItemRecyclerViewAdapter.notifyDataSetChanged();
                                    //OnItemClick
                                    chapterItemRecyclerViewAdapter.setOnItemClickListener(new ChapterItemRecyclerViewAdapter.OnItemClickListener() {
                                        @Override
                                        public void onItemClick(int position) {
                                            pushChapterData(position);
                                        }
                                    });
                                }
                                break;
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), "Error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("mangaid", _manga_id);

                return params;
            }
        };
        requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(stringRequest);
    }

    private void pushChapterData(int position){
        Chapter chapter = mChapter.get(position);

        Bundle bundle = new Bundle();
        bundle.putString("CHAPTER_ID", chapter.getId());
        bundle.putString("CHAPTER_TITLE", chapter.getTitle());
        bundle.putString("CHAPTER_THUMBNAIL", chapter.getThumbnail());
        bundle.putString("CHAPTER_CREATED_AT", chapter.getCreatedAt());
        bundle.putString("CHAPTER_UPDATED_AT", chapter.getUpdatedAt());
        bundle.putString("CHAPTER_CONTENT", chapter.getContent());
        bundle.putString("MANGA_TITLE", chapter.getMangaTitle());
        bundle.putString("CHAPTER_VIEWS", chapter.getViews());
        Fragment chapterFragment = new ChapterFragment();
        chapterFragment.setArguments(bundle);
        replaceFragment(chapterFragment);

    }

    public void replaceFragment(Fragment someFragment) {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.nav_host_fragment, someFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void init(View view) {
        thumbnail = view.findViewById(R.id.iv_manga_thumbnail);
        cover = view.findViewById(R.id.iv_manga_cover);
        title = view.findViewById(R.id.tv_manga_title);
        artist = view.findViewById(R.id.tv_manga_artist);
        createdAt = view.findViewById(R.id.tv_manga_created_at);
        summary = view.findViewById(R.id.tv_summary_content);
        thumbnail = view.findViewById(R.id.iv_manga_thumbnail);
        edtComment = view.findViewById(R.id.edt_comment);
        btnSend = view.findViewById(R.id.btn_send);
        rvCategoryList = view.findViewById(R.id.rv_category_list);
        rvChapterList = view.findViewById(R.id.rv_chapter_list);
        rvCommentList = view.findViewById(R.id.rv_comment_list);
        rvCategoryList.setHasFixedSize(true);
        rvChapterList.setHasFixedSize(true);
        rvCommentList.setHasFixedSize(true);
        rvCategoryList.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        rvChapterList.setLayoutManager(new LinearLayoutManager(getContext()));
        rvCommentList.setLayoutManager(new LinearLayoutManager(getContext()));
    }
}