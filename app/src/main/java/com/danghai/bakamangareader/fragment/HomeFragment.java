package com.danghai.bakamangareader.fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.danghai.bakamangareader.R;
import com.danghai.bakamangareader.adapter.MangaRecyclerViewAdapter;
import com.danghai.bakamangareader.model.Category;
import com.danghai.bakamangareader.model.Manga;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.danghai.bakamangareader.AppConfig.GET_MANGA_ORDER_BY_ID_URL;
import static com.danghai.bakamangareader.AppConfig.GET_NEW_MANGA_URL;
import static com.danghai.bakamangareader.AppConfig.GET_UPDATE_MANGA_URL;


public class HomeFragment extends Fragment {
    RecyclerView rvNewMangaList, rvUpdateMangaList;
    List<Manga> mMangaNew, mMangaUpdated;
    List<Category> mCategory;
    MangaRecyclerViewAdapter mangaRecyclerViewAdapter;
    StringRequest stringRequest;
    RequestQueue requestQueue;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        //Hooks
        init(view);
        loadNewMangaReleases();
        loadUpdateMangaReleases();

        return view;
    }

    private void loadNewMangaReleases() {
        stringRequest = new StringRequest(Request.Method.POST, GET_NEW_MANGA_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray items = new JSONArray(response);
                    for (int i = 0; i < items.length(); i++) {
                        JSONObject itemObj = items.getJSONObject(i);
                        String responseObj = itemObj.getString("response");
                        int statusObj = itemObj.getInt("status");

                        switch (statusObj){
                            case 400:
                                Toast.makeText(getContext(), "No Data", Toast.LENGTH_SHORT).show();
                                break;
                            case 200:
                                mCategory = new ArrayList<>();
                                mMangaNew = new ArrayList<>();
                                JSONArray newMangaItems = new JSONArray(responseObj);
                                for (int j = 0; j< newMangaItems.length(); j++) {
                                    JSONObject newMangaObj = newMangaItems.getJSONObject(j);
                                    String _id = newMangaObj.getString("id");
                                    String _title = newMangaObj.getString("title");
                                    String _thumbnail = newMangaObj.getString("thumbnail");
                                    String _cover = newMangaObj.getString("cover");
                                    String _createdAt = newMangaObj.getString("created_at");
                                    String _views = newMangaObj.getString("views");
                                    JSONArray categoryItems = new JSONArray(newMangaObj.getString("category"));
                                    for (int c = 0; c < categoryItems.length(); c++){
                                        JSONObject catObj = categoryItems.getJSONObject(c);
                                        String _catId = String.valueOf(catObj.getInt("cat_id"));
                                        String _catName = catObj.getString("name");
                                        mCategory.add(new Category(_catId, _catName));
                                    }
                                    mMangaNew.add(new Manga(_id, _title, _thumbnail, _cover, mCategory, _createdAt, _views));
                                    mangaRecyclerViewAdapter = new MangaRecyclerViewAdapter(getContext(), mMangaNew);
                                    rvNewMangaList.setAdapter(mangaRecyclerViewAdapter);
                                    mangaRecyclerViewAdapter.notifyDataSetChanged();
                                    mangaRecyclerViewAdapter.setOnItemClickListener(new MangaRecyclerViewAdapter.OnItemClickListener() {
                                        @Override
                                        public void onItemClick(int position) {
                                            pushDataMangaToFragment(position, mMangaNew);
                                        }
                                    });

                                }
                                break;
                        }

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), "Error: "+error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(stringRequest);

    }

    private void loadUpdateMangaReleases() {
        stringRequest = new StringRequest(Request.Method.POST, GET_UPDATE_MANGA_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray items = new JSONArray(response);
                    for (int i = 0; i < items.length(); i++) {
                        JSONObject itemObj = items.getJSONObject(i);
                        String responseObj = itemObj.getString("response");
                        int statusObj = itemObj.getInt("status");

                        switch (statusObj){
                            case 400:
                                Toast.makeText(getContext(), "No Data", Toast.LENGTH_SHORT).show();
                                break;
                            case 200:
                                mCategory = new ArrayList<>();
                                mMangaUpdated = new ArrayList<>();
                                JSONArray newMangaItems = new JSONArray(responseObj);
                                for (int j = 0; j< newMangaItems.length(); j++) {
                                    JSONObject newMangaObj = newMangaItems.getJSONObject(j);
                                    String _id = newMangaObj.getString("id");
                                    String _title = newMangaObj.getString("title");
                                    String _thumbnail = newMangaObj.getString("thumbnail");
                                    String _cover = newMangaObj.getString("cover");
                                    String _createdAt = newMangaObj.getString("created_at");
                                    String _views = newMangaObj.getString("views");
                                    JSONArray categoryItems = new JSONArray(newMangaObj.getString("category"));
                                    for (int c = 0; c < categoryItems.length(); c++){
                                        JSONObject catObj = categoryItems.getJSONObject(c);
                                        String _catId = String.valueOf(catObj.getInt("cat_id"));
                                        String _catName = catObj.getString("name");
                                        mCategory.add(new Category(_catId, _catName));
                                    }

                                    mMangaUpdated.add(new Manga(_id, _title, _thumbnail, _cover, mCategory, _createdAt, _views));
                                    mangaRecyclerViewAdapter = new MangaRecyclerViewAdapter(getContext(), mMangaUpdated);
                                    rvUpdateMangaList.setAdapter(mangaRecyclerViewAdapter);
                                    mangaRecyclerViewAdapter.notifyDataSetChanged();
                                    mangaRecyclerViewAdapter.setOnItemClickListener(new MangaRecyclerViewAdapter.OnItemClickListener() {
                                        @Override
                                        public void onItemClick(int position) {
                                            pushDataMangaToFragment(position, mMangaUpdated);
                                        }
                                    });

                                }
                                break;
                        }

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), "Error: "+error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(stringRequest);

    }

    private void pushDataMangaToFragment(final int position, final List<Manga> mManga) {
        stringRequest = new StringRequest(Request.Method.POST, GET_MANGA_ORDER_BY_ID_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray items = new JSONArray(response);
                    for (int i = 0; i < items.length(); i++) {
                        JSONObject itemObj = items.getJSONObject(i);
                        String responseObj = itemObj.getString("response");
                        int statusObj = itemObj.getInt("status");

                        switch (statusObj){
                            case 400:
                                Toast.makeText(getContext(), "No Data", Toast.LENGTH_SHORT).show();
                                break;
                            case 200:

                                JSONArray mangaDataItems = new JSONArray(responseObj);
                                for (int j = 0; j< mangaDataItems.length(); j++) {
                                    JSONObject newMangaObj = mangaDataItems.getJSONObject(j);
                                    String _id = newMangaObj.getString("id");
                                    String _title = newMangaObj.getString("title");
                                    String _content = newMangaObj.getString("content");
                                    String _artist = newMangaObj.getString("artist");
                                    String _thumbnail = newMangaObj.getString("thumbnail");
                                    String _cover = newMangaObj.getString("cover");
                                    String _createdAt = newMangaObj.getString("created_at");
                                    String _updatedAt = newMangaObj.getString("updated_at");
                                    String _views = newMangaObj.getString("views");

                                    JSONArray categoryItems = new JSONArray(newMangaObj.getString("category"));
                                    for (int c = 0; c < categoryItems.length(); c++) {
                                        JSONObject catObj = categoryItems.getJSONObject(c);
                                        String _catId = String.valueOf(catObj.getInt("cat_id"));
                                        String _catName = catObj.getString("name");
                                        mCategory.add(new Category(_catId, _catName));

                                    }
                                    Bundle bundle = new Bundle();
                                    bundle.putString("MANGA_ID", _id);
                                    bundle.putString("MANGA_TITLE", _title);
                                    bundle.putString("MANGA_THUMBNAIL", _thumbnail);
                                    bundle.putString("MANGA_COVER", _cover);
                                    bundle.putString("MANGA_CREATED_AT", _createdAt);
                                    bundle.putString("MANGA_UPDATED_AT", _updatedAt);
                                    bundle.putString("MANGA_CONTENT", _content);
                                    bundle.putString("MANGA_ARTIST", _artist);

                                    Fragment mangaFragment = new MangaFragment();
                                    mangaFragment.setArguments(bundle);
                                    replaceFragment(mangaFragment);

                                }
                                break;
                        }

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), "Error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                Manga manga = mManga.get(position);


                String txtMangaId = manga.getId();
                params.put("mangaid", txtMangaId);
                Log.d("txtMangaId", txtMangaId);
                return params;
            }
        };

        requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(stringRequest);

    }

    private void init(View view) {
        rvNewMangaList = view.findViewById(R.id.rv_new_manga_list);
        rvUpdateMangaList = view.findViewById(R.id.rv_update_manga_list);
        rvNewMangaList.setHasFixedSize(true);
        rvUpdateMangaList.setHasFixedSize(true);
        rvNewMangaList.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        rvUpdateMangaList.setLayoutManager(new GridLayoutManager(getContext(), 3));
    }

    public void replaceFragment(Fragment someFragment) {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.nav_host_fragment, someFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}

