package com.danghai.bakamangareader.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.danghai.bakamangareader.R;
import com.danghai.bakamangareader.adapter.CategoryRecyclerViewAdapter;
import com.danghai.bakamangareader.model.Category;
import com.google.android.material.textfield.TextInputLayout;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.app.Activity.RESULT_OK;
import static com.danghai.bakamangareader.AppConfig.DOMAIN_NAME;
import static com.danghai.bakamangareader.AppConfig.GET_ALL_CATEGORY_URL;
import static com.danghai.bakamangareader.AppConfig.INSERT_CATEGORY_URL;
import static com.danghai.bakamangareader.AppConfig.REMOVE_DATA_URL;
import static com.danghai.bakamangareader.AppConfig.REQUEST_CODE_GALLERY;
import static com.danghai.bakamangareader.AppConfig.UPDATE_CATEGORY_URL;
import static com.danghai.bakamangareader.lib.ExtraBakaLibs.imageToString;


public class CategoryControllerFragment extends Fragment {

    TextInputLayout categoryName, categoryDescription;
    ImageView cover;
    Button btnSubmit,btnUploadCover, btnUpdate;
    RecyclerView rvCategoryList;
    List<Category> mCategory;
    StringRequest stringRequest;
    RequestQueue requestQueue;
    String coverImageData;
    String coverImagePath;
    String catId;
    CategoryRecyclerViewAdapter categoryRecyclerViewAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_category_controller, container, false);
        //Hooks
        init(view);

        btnUploadCover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openImage();
            }
        });
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doInsertCategory();
            }
        });
        
        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doUpdateCategory();
            }
        });

        loadCategoryItems();


        return view;
    }
    private void doInsertCategory() {
        stringRequest = new StringRequest(Request.Method.POST, INSERT_CATEGORY_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray items = new JSONArray(response);
                    for (int i = 0; i < items.length(); i ++){
                        JSONObject itemObj = items.getJSONObject(i);
                        int statusObj = itemObj.getInt("status");
                        switch (statusObj){
                            case 403:
                                Toast.makeText(getContext(), "Not permission", Toast.LENGTH_SHORT).show();
                                break;
                            case 401:
                                Toast.makeText(getContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
                                break;
                            case 200:
                                coverImageData = "";
                                Toast.makeText(getContext(), "Insert Successfully", Toast.LENGTH_SHORT).show();
                                break;
                            case 500:
                                Toast.makeText(getContext(), "Cannot upload cover image", Toast.LENGTH_SHORT).show();
                                break;
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), "Error: " + error, Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("catname", categoryName.getEditText().getText().toString().trim());
                params.put("catdes",categoryDescription.getEditText().getText().toString().trim());
                params.put("catcover", coverImageData);
                return params;
            }
        };

        requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(stringRequest);


    }

    private void doUpdateCategory() {
      stringRequest = new StringRequest(Request.Method.POST, UPDATE_CATEGORY_URL, new Response.Listener<String>() {
          @Override
          public void onResponse(String response) {
              try {
                  JSONArray items = new JSONArray(response);
                  for (int i = 0; i < items.length(); i ++){
                      JSONObject itemObj = items.getJSONObject(i);
                      int statusObj = itemObj.getInt("status");
                      switch (statusObj){
                          case 403:
                              Toast.makeText(getContext(), "Not permission", Toast.LENGTH_SHORT).show();
                              break;
                          case 401:
                              Toast.makeText(getContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
                              break;
                          case 200:
                              btnSubmit.setVisibility(View.VISIBLE);
                              btnUpdate.setVisibility(View.GONE);
                              catId = "";
                              coverImagePath = "";
                              Toast.makeText(getContext(), "Update Successfully", Toast.LENGTH_SHORT).show();
                              break;
                          case 500:
                              Toast.makeText(getContext(), "Cannot upload cover image", Toast.LENGTH_SHORT).show();
                              break;
                      }
                  }
              } catch (JSONException e) {
                  e.printStackTrace();
              }
          }
      }, new Response.ErrorListener() {
          @Override
          public void onErrorResponse(VolleyError error) {
              Toast.makeText(getContext(), "Error: " + error, Toast.LENGTH_SHORT).show();
          }
      }){
          @Override
          protected Map<String, String> getParams() throws AuthFailureError {
              Map<String, String> params =  new HashMap<>();
              params.put("catid", catId);
              params.put("catname", categoryName.getEditText().getText().toString().trim());
              params.put("catdes",categoryDescription.getEditText().getText().toString().trim());
              if (!TextUtils.isEmpty(coverImageData)) {
                  params.put("catcover", coverImageData);
              }
              params.put("catcoverpath", coverImagePath);
              return params;
          }
      };

      requestQueue = Volley.newRequestQueue(getContext());
      requestQueue.add(stringRequest);

    }

    private void doDeleteCategory(int position) {
        final Category category = mCategory.get(position);
        catId = category.getId();
        stringRequest = new StringRequest(Request.Method.POST, REMOVE_DATA_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray items = new JSONArray(response);
                    for (int i = 0; i < items.length(); i ++){
                        JSONObject itemObj = items.getJSONObject(i);
                        String responseObj = itemObj.getString("response");
                        int statusObj = itemObj.getInt("status");
                        switch (statusObj){
                            case 403:
                                Toast.makeText(getContext(), "Not permission", Toast.LENGTH_SHORT).show();
                                break;
                            case 401:
                                Toast.makeText(getContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
                                break;
                            case 200:
                                mCategory.remove(category);
                                categoryRecyclerViewAdapter.notifyDataSetChanged();
                                rvCategoryList.setAdapter(categoryRecyclerViewAdapter);
                                Toast.makeText(getContext(), "Remove category successfully", Toast.LENGTH_SHORT).show();
                                break;
                        }
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), "Error: " + error, Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("catid", catId);
                return params;
            }
        };

        requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(stringRequest);


    }

    private void loadCategoryItems() {
        mCategory = new ArrayList<>();
        stringRequest = new StringRequest(Request.Method.POST, GET_ALL_CATEGORY_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray items = new JSONArray(response);
                    for (int i = 0; i < items.length(); i++) {
                        JSONObject itemObj = items.getJSONObject(i);
                        String responseObj = itemObj.getString("response");
                        int statusObj = itemObj.getInt("status");
                        switch (statusObj){
                            case 400:
                                Toast.makeText(getContext(), "No Data", Toast.LENGTH_SHORT).show();
                                break;
                            case 200:
                                JSONArray categoryItem = new JSONArray(responseObj);
                                for (int j = 0; j< categoryItem.length(); j++){
                                    JSONObject categoryObj = categoryItem.getJSONObject(j);
                                    String _id = String.valueOf(categoryObj.getInt("cat_id"));
                                    String _name = categoryObj.getString("name");
                                    String _des = categoryObj.getString("des");
                                    String _image = categoryObj.getString("image");
                                    String _createdAt = categoryObj.getString("created_at");
                                    String _updatedAt = categoryObj.getString("updated_at");
                                    mCategory.add(new Category(_id, _name, _des, _image, _createdAt, _updatedAt));
                                    categoryRecyclerViewAdapter = new CategoryRecyclerViewAdapter(getContext(), mCategory);
                                    rvCategoryList.setAdapter(categoryRecyclerViewAdapter);
                                    categoryRecyclerViewAdapter.notifyDataSetChanged();
                                }
                                //Onclick
                                categoryRecyclerViewAdapter.setOnItemClickListener(new CategoryRecyclerViewAdapter.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(int position) {
                                        btnSubmit.setVisibility(View.GONE);
                                        btnUpdate.setVisibility(View.VISIBLE);
                                        Category category = mCategory.get(position);
                                        categoryName.getEditText().setText(category.getName());
                                        categoryDescription.getEditText().setText(category.getDes());
                                        Picasso.get().load(DOMAIN_NAME + category.getImage()).fit().into(cover);
                                        coverImagePath = category.getImage();
                                        catId = category.getId();
                                    }

                                });


                                break;
                        }


                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), "Error: " + error, Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return super.getParams();
            }
        };

        requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(stringRequest);
    }

    private void openImage() {
        Intent galleryIntent = new Intent();
        galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        startActivityForResult(galleryIntent, REQUEST_CODE_GALLERY);
    }

    private void init(View view) {
        categoryName = view.findViewById(R.id.edt_category_name);
        categoryDescription = view.findViewById(R.id.edt_category_des);
        cover = view.findViewById(R.id.iv_category_cover);
        btnUploadCover = view.findViewById(R.id.btn_upload_cover);
        btnSubmit = view.findViewById(R.id.btn_category_submit);
        btnUpdate = view.findViewById(R.id.btn_category_update);
        rvCategoryList = view.findViewById(R.id.rv_category_controller_list);
        rvCategoryList.setHasFixedSize(true);
        rvCategoryList.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater menuInflater = getActivity().getMenuInflater();
        menuInflater.inflate(R.menu.category_menu, menu);

    }

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            case R.id.nav_category_delete:
                doDeleteCategory(info.position);
                break;
        }
        return super.onContextItemSelected(item);
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == REQUEST_CODE_GALLERY) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent, REQUEST_CODE_GALLERY);
            } else {
                Toast.makeText(getContext(), "Error! You must have to permission!", Toast.LENGTH_SHORT).show();
            }
            return;
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        if (requestCode == REQUEST_CODE_GALLERY && resultCode == RESULT_OK && data != null) {

            CropImage.activity()
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .setAspectRatio(4, 1)
                    .start(getContext(), this);
        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);

            if (resultCode == RESULT_OK) {

                assert result != null;
                Uri resultUri = result.getUri();
                try {
                    InputStream inputStream = requireActivity().getApplicationContext().getContentResolver().openInputStream(resultUri);
                    Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                    cover.setImageBitmap(bitmap);
                    coverImageData = imageToString(bitmap);


                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }

            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}