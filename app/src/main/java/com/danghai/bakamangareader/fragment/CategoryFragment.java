package com.danghai.bakamangareader.fragment;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.danghai.bakamangareader.R;
import com.danghai.bakamangareader.adapter.CategoryRecyclerViewAdapter;
import com.danghai.bakamangareader.model.Category;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.danghai.bakamangareader.AppConfig.GET_ALL_CATEGORY_URL;


public class CategoryFragment extends Fragment {

    RecyclerView rvCategoryList;
    List<Category> mList;
    CategoryRecyclerViewAdapter categoryRecyclerViewAdapter;
    StringRequest stringRequest;
    RequestQueue requestQueue;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_category, container, false);

        //Hooks
        init(view);
        mList = new ArrayList<>();
        stringRequest = new StringRequest(Request.Method.POST, GET_ALL_CATEGORY_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray items = new JSONArray(response);
                    for (int i = 0; i < items.length(); i++) {
                        JSONObject itemObj = items.getJSONObject(i);
                        String responseObj = itemObj.getString("response");
                        int statusObj = itemObj.getInt("status");
                        switch (statusObj){
                            case 400:
                                Toast.makeText(getContext(), "No Data", Toast.LENGTH_SHORT).show();
                                break;
                            case 200:
                                JSONArray categoryItem = new JSONArray(responseObj);
                                for (int j = 0; j< categoryItem.length(); j++){
                                    JSONObject categoryObj = categoryItem.getJSONObject(j);
                                    String _id = String.valueOf(categoryObj.getInt("cat_id"));
                                    String _name = categoryObj.getString("name");
                                    String _des = categoryObj.getString("des");
                                    String _image = categoryObj.getString("image");
                                    String _createdAt = categoryObj.getString("created_at");
                                    String _updatedAt = categoryObj.getString("updated_at");
                                   mList.add(new Category(_id, _name, _des, _image, _createdAt, _updatedAt));
                                    categoryRecyclerViewAdapter = new CategoryRecyclerViewAdapter(getContext(), mList);
                                    rvCategoryList.setAdapter(categoryRecyclerViewAdapter);
                                    categoryRecyclerViewAdapter.notifyDataSetChanged();
                                }

                                //Onclick
                                categoryRecyclerViewAdapter.setOnItemClickListener(new CategoryRecyclerViewAdapter.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(int position) {
                                        pushCategoriesData(position, mList);
                                    }

                                });


                                break;
                        }


                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(stringRequest);


        return view;
    }

    private void pushCategoriesData(int position, List<Category> mCategory){
        Category category = mList.get(position);
        Bundle bundle = new Bundle();

        Fragment mangaFragment = new MangaOrderByCategoryFragment();
        mangaFragment.setArguments(bundle);
        replaceFragment(mangaFragment);



    }

    private void init(View view) {
        rvCategoryList = view.findViewById(R.id.rv_category_list);
        rvCategoryList.setHasFixedSize(true);
        rvCategoryList.setLayoutManager(new LinearLayoutManager(getContext()));

    }
    public void replaceFragment(Fragment someFragment) {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.nav_host_fragment, someFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}