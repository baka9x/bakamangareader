package com.danghai.bakamangareader.fragment;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.danghai.bakamangareader.R;
import com.danghai.bakamangareader.adapter.ChapterImageViewAdapter;
import com.danghai.bakamangareader.model.Chapter;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import static com.danghai.bakamangareader.AppConfig.DOMAIN_NAME;


public class ChapterFragment extends Fragment {

    RelativeLayout relativeLayout;
    LinearLayout linearLayout;

    ImageView ivThumbnail;
    TextView tvMangaTitle, tvChapterTitle;
    Button btnPrevChapter, btnNextChapter, btnListChapter, btnBack;
    RecyclerView rvChapterList;
    StringRequest stringRequest;
    RequestQueue requestQueue;
    List<Chapter> mChapter;
    ChapterImageViewAdapter chapterImageViewAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_chapter, container, false);
        //Hooks
        init(view);

        loadDataChapter();

        return view;
    }



    private void loadDataChapter() {
        mChapter = new ArrayList<>();
        Bundle bundle = getArguments();
        String _chapter_id = bundle.getString("CHAPTER_ID");
        String _chapter_title = bundle.getString("CHAPTER_TITLE");
        String _chapter_thumbnail = bundle.getString("CHAPTER_THUMBNAIL");
        String _chapter_created = bundle.getString("CHAPTER_CREATED_AT");
        String _chapter_updated = bundle.getString("CHAPTER_UPDATED_AT");
        String _chapter_content = bundle.getString("CHAPTER_CONTENT");
        String _chapter_views = bundle.getString("CHAPTER_VIEWS");
        String _manga_title = bundle.getString("MANGA_TITLE");
        //Set data to layout
        Picasso.get().load(DOMAIN_NAME + _chapter_thumbnail).fit().into(ivThumbnail);
        tvMangaTitle.setText(_manga_title);
        tvChapterTitle.setText(_chapter_title);
        assert _chapter_content != null;
        String[] chapterImage = _chapter_content.split(",");
        for (int i = 0; i < chapterImage.length; i++){
            mChapter.add(new Chapter(chapterImage[i]));
            Log.d("chapterImage", chapterImage[i]);
        }
        chapterImageViewAdapter = new ChapterImageViewAdapter(getContext(), mChapter);
        rvChapterList.setAdapter(chapterImageViewAdapter);
        chapterImageViewAdapter.notifyDataSetChanged();
        rvChapterList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) {

                } else {
                    linearLayout.setVisibility(View.VISIBLE);
                    relativeLayout.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                switch (newState ){
                    case AbsListView.OnScrollListener.SCROLL_STATE_FLING:
                        linearLayout.setVisibility(View.GONE);
                        relativeLayout.setVisibility(View.GONE);
                    break;

                    case AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL:

                    break;

                }
            }
        });


    }

    private void init(View view) {
        relativeLayout = view.findViewById(R.id.relative_layout);
        linearLayout = view.findViewById(R.id.linear_layout);
        ivThumbnail = view.findViewById(R.id.iv_logo);
        tvMangaTitle = view.findViewById(R.id.tv_manga_title);
        tvChapterTitle = view.findViewById(R.id.tv_chapter_title);
        btnPrevChapter = view.findViewById(R.id.btn_prev_chapter);
        btnNextChapter = view.findViewById(R.id.btn_next_chapter);
        btnListChapter = view.findViewById(R.id.btn_list_chapter);
        btnBack = view.findViewById(R.id.btn_back);
        rvChapterList = view.findViewById(R.id.rv_chapter_data_list);
        rvChapterList.setHasFixedSize(true);
        rvChapterList.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    @Override
    public void onResume() {
        super.onResume();
        ((AppCompatActivity)getActivity()).getSupportActionBar().hide();
    }
    @Override
    public void onStop() {
        super.onStop();
        ((AppCompatActivity)getActivity()).getSupportActionBar().show();
    }
}