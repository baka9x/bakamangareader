package com.danghai.bakamangareader.fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.danghai.bakamangareader.R;
import com.danghai.bakamangareader.adapter.CategoryRecyclerViewAdapter;
import com.danghai.bakamangareader.adapter.HomeCategoryViewPagerAdapter;
import com.danghai.bakamangareader.model.Category;
import com.danghai.bakamangareader.model.Manga;
import com.google.android.material.tabs.TabLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.danghai.bakamangareader.AppConfig.GET_ALL_CATEGORY_URL;

public class MangaOrderByCategoryFragment extends Fragment {
    List<Category> mCategory;
    List<Manga> mManga;
    TabLayout tabLayout;
    ViewPager viewPager;
    HomeCategoryViewPagerAdapter homeCategoryViewPagerAdapter;

    StringRequest stringRequest;
    RequestQueue requestQueue;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // setHasOptionsMenu(true);
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_manga_order_by_category, container, false);
        //Hooks
        init(view);
        loadMangaOrderByCategory();
        
        return view;
    }

    private void loadMangaOrderByCategory() {

        stringRequest = new StringRequest(Request.Method.POST, GET_ALL_CATEGORY_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray items = new JSONArray(response);
                    for (int i = 0; i < items.length(); i++) {
                        JSONObject itemObj = items.getJSONObject(i);
                        String responseObj = itemObj.getString("response");
                        int statusObj = itemObj.getInt("status");
                        switch (statusObj){
                            case 400:
                                Toast.makeText(getContext(), "No Data", Toast.LENGTH_SHORT).show();
                                break;
                            case 200:
                                JSONArray categoryItem = new JSONArray(responseObj);
                                for (int j = 0; j< categoryItem.length(); j++){
                                    JSONObject categoryObj = categoryItem.getJSONObject(j);
                                    String _id = String.valueOf(categoryObj.getInt("cat_id"));
                                    String _name = categoryObj.getString("name");
                                    String _des = categoryObj.getString("des");
                                    String _image = categoryObj.getString("image");
                                    String _createdAt = categoryObj.getString("created_at");
                                    String _updatedAt = categoryObj.getString("updated_at");
                                    mCategory.add(new Category(_id, _name, _des, _image, _createdAt, _updatedAt));
                                    for (int ii = 0; ii < mCategory.size(); ii++) {
                                        homeCategoryViewPagerAdapter.addFragment(new MangaFragment(mCategory.get(i).getId()), mCategory.get(i).getName());
                                        homeCategoryViewPagerAdapter.notifyDataSetChanged();

                                    }
                                    viewPager.setAdapter(homeCategoryViewPagerAdapter);
                                    tabLayout.setupWithViewPager(viewPager);
                                }

                                break;
                        }


                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(stringRequest);
    }

    private void init(View view) {
        mCategory = new ArrayList<>();
        mManga = new ArrayList<>();
        //Tab
        tabLayout = view.findViewById(R.id.tab_layout);
        viewPager = view.findViewById(R.id.view_pager);
        homeCategoryViewPagerAdapter = new HomeCategoryViewPagerAdapter(getChildFragmentManager());
    }
}