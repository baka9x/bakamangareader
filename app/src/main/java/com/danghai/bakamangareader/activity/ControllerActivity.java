package com.danghai.bakamangareader.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.MenuItem;

import com.danghai.bakamangareader.R;
import com.danghai.bakamangareader.fragment.AnalyticsFragment;
import com.danghai.bakamangareader.fragment.CategoryControllerFragment;
import com.danghai.bakamangareader.fragment.ChapterControllerFragment;
import com.danghai.bakamangareader.fragment.MangaControllerFragment;
import com.danghai.bakamangareader.fragment.ToolLeechFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class ControllerActivity extends AppCompatActivity {
    Toolbar toolbar;
    BottomNavigationView bottomNav;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_controller);
        //Hooks
        init();
    }

    private void init() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(Color.parseColor("#FFEB3B"));
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        bottomNav = findViewById(R.id.nav_view);
        bottomNav.setOnNavigationItemSelectedListener(navListener);
    }

    private BottomNavigationView.OnNavigationItemSelectedListener navListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                    Fragment selectFragment = null;
                    switch (menuItem.getItemId()) {
                        case R.id.nav_home:
                            getSupportActionBar().setTitle("Analytics");
                            selectFragment = new AnalyticsFragment();
//                            Intent intent = new Intent(ControllerActivity.this, SplashActivity.class);
//                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//                            startActivity(intent);
//                            finish();
                            break;
                        case R.id.nav_category:
                            getSupportActionBar().setTitle("Category Controller");
                            selectFragment = new CategoryControllerFragment();
                            break;
                        case R.id.nav_chapter:
                            getSupportActionBar().setTitle("Chapter Controller");
                            selectFragment = new ChapterControllerFragment();
                            break;
                        case R.id.nav_comic:
                            getSupportActionBar().setTitle("Manga Controller");
                            selectFragment = new MangaControllerFragment();
                            break;
                        case R.id.nav_leech:
                            getSupportActionBar().setTitle("Tool Leech");
                            selectFragment = new ToolLeechFragment();
                            break;
                    }
                    //Run Default Fragment
                    if (selectFragment == null){
                        selectFragment = new AnalyticsFragment();
                    }
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, selectFragment).commit();
                    return true;


                }
            };
}