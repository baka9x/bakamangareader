package com.danghai.bakamangareader.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.danghai.bakamangareader.R;
import com.google.android.material.navigation.NavigationView;
import com.squareup.picasso.Picasso;

import androidx.annotation.FontRes;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.danghai.bakamangareader.AppConfig.DOMAIN_NAME;
import static com.danghai.bakamangareader.AppConfig.GET_CURRENT_USER_URL;
import static com.danghai.bakamangareader.AppConfig.LOGOUT_URL;
import static com.danghai.bakamangareader.AppConfig.MY_PERMISSIONS_REQUEST_READ_STORAGE;
import static com.danghai.bakamangareader.AppConfig.MY_PERMISSIONS_REQUEST_WRITE_STORAGE;
import static com.danghai.bakamangareader.AppConfig.REQUEST_CODE_GALLERY;

public class MainActivity extends AppCompatActivity {

    private AppBarConfiguration mAppBarConfiguration;

    TextView usernameLoggedIn, fullName;

    CircleImageView avatar;

    SharedPreferences sharedPreferences;

    @RequiresApi(api = Build.VERSION_CODES.R)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        myWriteStorageRequestPermission();
        myReadStorageRequestPermission();
        myMediaGalleryPermission();

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);


        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home,R.id.nav_category,  R.id.nav_gallery, R.id.nav_slideshow)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);


        View headerView = navigationView.getHeaderView(0);
        usernameLoggedIn = headerView.findViewById(R.id.tv_username_logged_in);
        fullName = headerView.findViewById(R.id.tv_full_name);
        avatar = headerView.findViewById(R.id.avatar);

        sharedPreferences = getSharedPreferences("LoginInfo", Context.MODE_PRIVATE);
        //Nạp thông tin lên form từ sharedPreference
        Boolean dataSave = sharedPreferences.getBoolean("login_information", false);
        if (dataSave) {
            usernameLoggedIn.setText(sharedPreferences.getString("username", ""));

            getCurrentUserOnline();
        }else{
            Intent intent = new Intent(MainActivity.this, LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }

    }


    private void getCurrentUserOnline() {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, GET_CURRENT_USER_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray items = new JSONArray(response);

                    for (int i = 0; i < items.length(); i++) {
                        JSONObject itemObj = items.getJSONObject(i);
                        String responseObj = itemObj.getString("response");
                        int statusObj = itemObj.getInt("status");
                        switch (statusObj){
                            case 403:
                                Toast.makeText(MainActivity.this, "Not permission", Toast.LENGTH_SHORT).show();
                                break;
                            case 404:
                                Toast.makeText(MainActivity.this, "No user data", Toast.LENGTH_SHORT).show();
                                break;
                            case 200:
                                JSONArray userItem = new JSONArray(responseObj);
                                for (int j = 0; j < userItem.length(); j++) {
                                    JSONObject userObj = userItem.getJSONObject(i);
                                    int id = userObj.getInt("id");
                                    String _fullname = userObj.getString("fullname");
                                    String _username = userObj.getString("username");
                                    String _email = userObj.getString("email");
                                    int _isOnline = userObj.getInt("is_online");
                                    String _createdAt = userObj.getString("created_at");
                                    int _type = userObj.getInt("type");
                                    String _avatar = userObj.getString("avatar");

                                    //Set data to layout
                                    fullName.setText(_fullname);

                                    Picasso.get().load(DOMAIN_NAME + _avatar).fit().into(avatar);
                                }
                                break;

                        }

                    }



                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("username", sharedPreferences.getString("username", ""));

                return params;

            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
        requestQueue.add(stringRequest);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_settings:
                Intent intent = new Intent(MainActivity.this, ControllerActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
               return true;
            case R.id.action_logout:
                logoutCurrentUser();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void logoutCurrentUser() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, LOGOUT_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray items = new JSONArray(response);
                    for (int i = 0; i < items.length(); i++){
                        JSONObject itemObj = items.getJSONObject(i);
                        //String reponseObj = itemObj.getString("response");
                        int statusObj = itemObj.getInt("status");
                        switch (statusObj){
                            case 403:
                            case 400:
                                Toast.makeText(MainActivity.this, "Something is Wrong", Toast.LENGTH_SHORT).show();
                                break;
                            case 200:
                                Toast.makeText(MainActivity.this, "Logout Successfully", Toast.LENGTH_SHORT).show();
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putString("username", "");
                                editor.putBoolean("login_information", false);
                                editor.apply();
                                Intent intent = new Intent(MainActivity.this, SplashActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                                finish();
                                break;
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(MainActivity.this, "Error: "+ error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("username", sharedPreferences.getString("username", ""));
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
        requestQueue.add(stringRequest);

    }
    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    @RequiresApi(api = Build.VERSION_CODES.R)
    private void myReadStorageRequestPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {

            } else {

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_READ_STORAGE);
            }
        } else {

        }
    }

    @RequiresApi(api = Build.VERSION_CODES.R)
    private void myWriteStorageRequestPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_WRITE_STORAGE);
                //To do list when have write external storage permission
            }
        } else {

        }
    }

    @RequiresApi(api = Build.VERSION_CODES.R)
    private void myMediaGalleryPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {

            } else {

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        REQUEST_CODE_GALLERY);
            }
        } else {

        }
    }


}