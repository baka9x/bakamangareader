package com.danghai.bakamangareader.activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.ActivityOptions;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Pair;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.danghai.bakamangareader.R;

import static com.danghai.bakamangareader.AppConfig.MY_PERMISSIONS_REQUEST_READ_STORAGE;
import static com.danghai.bakamangareader.AppConfig.MY_PERMISSIONS_REQUEST_WRITE_STORAGE;
import static com.danghai.bakamangareader.AppConfig.REQUEST_CODE_GALLERY;

public class SplashActivity extends AppCompatActivity {

    private static int SPLASH_SCREEN= 2000;

    Animation topAnim, bottomAnim;
    ImageView ivLogo;
    TextView tvLogo, tvSlogan;
    @RequiresApi(api = Build.VERSION_CODES.R)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);
        //Animation
        topAnim = AnimationUtils.loadAnimation(this,R.anim.top_anim );
        bottomAnim = AnimationUtils.loadAnimation(this,R.anim.bottom_anim );
        //Hooks
        ivLogo = findViewById(R.id.iv_logo);
        tvLogo = findViewById(R.id.tv_logo_title);
        tvSlogan = findViewById(R.id.tv_slogan_title);


        ivLogo.setAnimation(topAnim);
        tvLogo.setAnimation(bottomAnim);
        tvSlogan.setAnimation(bottomAnim);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                Pair[] pairs = new Pair[2];
                pairs[0] = new Pair<View, String>(ivLogo, "logo_image");
                pairs[1] = new Pair<View, String>(tvLogo, "logo_text");

                ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(SplashActivity.this, pairs);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent, options.toBundle());
                finish();
            }
        }, SPLASH_SCREEN);
    }




}