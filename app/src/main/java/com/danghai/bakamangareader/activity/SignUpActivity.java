package com.danghai.bakamangareader.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.danghai.bakamangareader.R;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.danghai.bakamangareader.AppConfig.REGISTER_URL;

public class SignUpActivity extends AppCompatActivity {

    TextInputLayout fullname, username, email, password, rePassword;
    Button btnSignUp, btnLogin;
    ImageView ivLogo;
    TextView tvLogo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_sign_up);
        //Hooks
        init();

        //Button Event
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SignUpActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });

        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!validateFullName() | !validateUsername() | !validateEmail() | !validatePassword() | !validateRePassword()) {
                    Toast.makeText(SignUpActivity.this, "Please check your information", Toast.LENGTH_SHORT).show();
                    return;
                }
                StringRequest stringRequest = new StringRequest(Request.Method.POST, REGISTER_URL, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray items = new JSONArray(response);
                            for (int i = 0; i < items.length(); i++) {
                                JSONObject itemObj = items.getJSONObject(i);

                                //String reponseObj = itemObj.getString("response");
                                int statusObj = itemObj.getInt("status");
                                switch (statusObj) {
                                    case 403:
                                        Toast.makeText(SignUpActivity.this, "Please check your information", Toast.LENGTH_SHORT).show();
                                        break;
                                    case 401:
                                        Toast.makeText(SignUpActivity.this, "Username is exist", Toast.LENGTH_SHORT).show();
                                        break;
                                    case 402:
                                        Toast.makeText(SignUpActivity.this, "Email is exist", Toast.LENGTH_SHORT).show();
                                        break;
                                    case 200:
                                        Toast.makeText(SignUpActivity.this, "Register successfully", Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(SignUpActivity.this, LoginActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                        finish();
                                        break;
                                    case 500:
                                        Toast.makeText(SignUpActivity.this, "Something is Wrong", Toast.LENGTH_SHORT).show();
                                        break;
                                }
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(SignUpActivity.this, "Error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<>();
                        String _fullName = fullname.getEditText().getText().toString().trim();
                        String _username = username.getEditText().getText().toString().trim();
                        String _password = password.getEditText().getText().toString().trim();
                        String _email = email.getEditText().getText().toString().trim();
                        params.put("fullname", _fullName);
                        params.put("username", _username);
                        params.put("password", _password);
                        params.put("email", _email);
                        return params;
                    }
                };
                RequestQueue requestQueue = Volley.newRequestQueue(SignUpActivity.this);
                requestQueue.add(stringRequest);

            }

        });
    }

    private Boolean validateUsername() {
        String val = username.getEditText().getText().toString().trim();
        String noWhiteSpace = "\\A\\w{4,20}\\z";
        if (val.isEmpty()) {
            username.setError("Fields is not empty");
            return false;
        } else if (val.length() > 16 || val.length() < 6) {
            username.setError("The username length is from 6 to 16 characters");
            return false;
        } else if (!val.matches(noWhiteSpace)) {
            username.setError("Username cannot contain spaces");
            return false;
        } else {
            username.setError(null);
            username.setErrorEnabled(false);
            return true;
        }

    }

    private Boolean validateFullName() {
        String val = fullname.getEditText().getText().toString().trim();
        if (val.isEmpty()) {
            fullname.setError("Fields is not empty");
            return false;
        } else if (val.length() > 16 || val.length() < 6) {
            fullname.setError("The full name length is from 6 to 16 characters");
            return false;
        } else {
            fullname.setError(null);
            fullname.setErrorEnabled(false);
            return true;
        }
    }

    private Boolean validateEmail() {
        String val = email.getEditText().getText().toString().trim();
        String isEmailExp = "^[a-z][a-z0-9_\\.]{5,32}@[a-z0-9]{2,}(\\.[a-z0-9]{2,4}){1,2}$";
        if (val.isEmpty()) {
            email.setError("Fields is not empty");
            return false;
        } else if (!val.matches(isEmailExp)) {
            email.setError("Email invalidate");
            return false;
        } else {
            email.setError(null);
            email.setErrorEnabled(false);
            return true;
        }
    }

    private Boolean validatePassword() {
        String val = password.getEditText().getText().toString().trim();
//        String passwordVal = "^" +
////                "(?=.*[0-9])" +           //at least 1 digit
////                "(?=.*[a-z])" +           //at least 1 lower case letter
////                "(?=.*[A-Z])" +           //at least 1 upper case letter
////                "(?=.*[a-zA-Z])" +          //any letter
////                "(?=.*[@#$%^&+=])" +        //at least 1 special character
////                "\A\w{4,20}\z" +             //no white space
////                ".{4, }" +                  //at least 4 characters
////                "$";
        if (val.isEmpty()) {
            password.setError("Field is not empty");
            return false;
        } else if (val.length() > 16 || val.length() < 8) {
            password.setError("The password length is from 8 to 16 characters");
            return false;
        } else {
            password.setError(null);
            password.setErrorEnabled(false);
            return true;
        }

    }

    private Boolean validateRePassword() {
        String val = rePassword.getEditText().getText().toString().trim();
        if (val.isEmpty()) {
            rePassword.setError("Fields is not empty");
            return false;
        } else if (!val.contains(password.getEditText().getText().toString().trim())) {
            rePassword.setError("The repeat password do not match with password");
            return false;
        } else {
            rePassword.setError(null);
            rePassword.setErrorEnabled(false);
            return true;
        }
    }

    private void init() {
        ivLogo = findViewById(R.id.iv_logo_sign_up);
        tvLogo = findViewById(R.id.tv_sign_up_title);
        fullname = findViewById(R.id.sign_up_full_name);
        username = findViewById(R.id.sign_up_username);
        email = findViewById(R.id.sign_up_email);
        password = findViewById(R.id.sign_up_password);
        rePassword = findViewById(R.id.sign_up_re_password);
        btnLogin = findViewById(R.id.btn_login);
        btnSignUp = findViewById(R.id.btn_sign_up);
    }
}