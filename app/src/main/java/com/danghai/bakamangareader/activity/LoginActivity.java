package com.danghai.bakamangareader.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.danghai.bakamangareader.R;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.danghai.bakamangareader.AppConfig.LOGIN_URL;

public class LoginActivity extends AppCompatActivity {

    TextInputLayout username, password;
    Button btnLogin, btnSignUp, btnForgotPassword;
    ImageView ivLogo;
    TextView tvLogo;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login);
        //Hooks
        init();
        loginInfo();
        //Action Button
        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, SignUpActivity.class);
                startActivity(intent);
                finish();
            }
        });
        btnForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!validateUsername() | !validatePassword()){
                    Toast.makeText(LoginActivity.this, "Please check your login information", Toast.LENGTH_SHORT).show();
                    return;
                }
                final String _username = username.getEditText().getText().toString().trim();
                final String _password = password.getEditText().getText().toString().trim();

                StringRequest stringRequest = new StringRequest(Request.Method.POST, LOGIN_URL, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray items = new JSONArray(response);
                            for (int i = 0; i < items.length(); i++) {
                                JSONObject itemObj = items.getJSONObject(i);
                                //String reponseObj = itemObj.getString("response");
                                int statusObj = itemObj.getInt("status");
                                switch (statusObj){
                                    case 403:
                                        Toast.makeText(LoginActivity.this, "Please check your information", Toast.LENGTH_SHORT).show();
                                        break;
                                    case 400:
                                        Toast.makeText(LoginActivity.this, "Error Login username", Toast.LENGTH_SHORT).show();
                                        break;
                                    case 500:
                                        Toast.makeText(LoginActivity.this, "Something is Wrong", Toast.LENGTH_SHORT).show();
                                        break;
                                    case 200:
                                        Toast.makeText(LoginActivity.this, "Login Successfully", Toast.LENGTH_SHORT).show();
                                        SharedPreferences.Editor editor = sharedPreferences.edit();
                                        editor.putString("username", _username);
                                        editor.putBoolean("login_information", true);
                                        editor.apply();
                                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                        finish();
                                        break;
                                }

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(LoginActivity.this, "Error: "+error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<>();
                        params.put("username", _username);
                        params.put("password", _password);
                        return params;
                    }
                };

                RequestQueue requestQueue = Volley.newRequestQueue(LoginActivity.this);
                requestQueue.add(stringRequest);


            }
        });

    }

    private Boolean validateUsername(){
        String val = username.getEditText().getText().toString().trim();
        String noWhiteSpace = "\\A\\w{4,20}\\z";
        if (val.isEmpty()){
            username.setError("Fields is not empty");
            return false;
        }else if(val.length() > 16 || val.length() < 6){
            username.setError("The username length is from 6 to 16 characters");
            return false;
        }else if (!val.matches(noWhiteSpace)){
            username.setError("Username cannot contain spaces");
            return false;
        }
        else{
            username.setError(null);
            username.setErrorEnabled(false);
            return true;
        }

    }

    private Boolean validatePassword(){
        String val = password.getEditText().getText().toString().trim();
//        String passwordVal = "^" +
////                "(?=.*[0-9])" +           //at least 1 digit
////                "(?=.*[a-z])" +           //at least 1 lower case letter
////                "(?=.*[A-Z])" +           //at least 1 upper case letter
////                "(?=.*[a-zA-Z])" +          //any letter
////                "(?=.*[@#$%^&+=])" +        //at least 1 special character
////                "\A\w{4,20}\z" +             //no white space
////                ".{4, }" +                  //at least 4 characters
////                "$";
        if (val.isEmpty()){
            password.setError("Field is not empty");
            return false;
        }else if (val.length() > 16 || val.length() < 8){
            password.setError("The password length is from 8 to 16 characters");
            return false;
        }
        else{
            password.setError(null);
            password.setErrorEnabled(false);
            return true;
        }

    }

    private void init() {
        ivLogo = findViewById(R.id.iv_logo_login);
        tvLogo = findViewById(R.id.tv_login_title);
        username = findViewById(R.id.login_username);
        password = findViewById(R.id.login_password);
        btnLogin = findViewById(R.id.btn_login);
        btnSignUp = findViewById(R.id.btn_sign_up);
        btnForgotPassword = findViewById(R.id.btn_forgot_password);
    }

    public void loginInfo(){
        sharedPreferences = getSharedPreferences("LoginInfo", Context.MODE_PRIVATE);
        //Nạp thông tin lên form từ sharedPreference
        Boolean dataSave = sharedPreferences.getBoolean("login_information", false);
    }
}